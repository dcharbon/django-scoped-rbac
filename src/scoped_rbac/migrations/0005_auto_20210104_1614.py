# Generated by Django 2.2.17 on 2021-01-04 16:14

from django.db import migrations, models
from uuid import uuid4
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('scoped_rbac', '0004_auto_20200223_1921'),
    ]

    operations = [
        migrations.AddField(
            model_name='role',
            name='etag',
            field=models.CharField(default=uuid4().hex, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='role',
            name='modified_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='roleassignment',
            name='etag',
            field=models.CharField(default=uuid4().hex, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='roleassignment',
            name='modified_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
