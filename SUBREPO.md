# `git-subrepo`
We're using [git-subrepo](https://github.com/ingydotnet/git-subrepo) to manage the
[cel-python](https://github.com/cloud-custodian/cel-python) dependency until
[issue-13](https://github.com/cloud-custodian/cel-python/issues/13) is resolved and we can use the pypi.org package.

To manage our version of `cel-python`, install `git-subrepo`.